# pyTelegramBotApi_bots 
Боты написанные с помощью библиотеки pyTelegramBotApi

### На каких ботов обратить внимание:
- [Guess tune bot](#guess_tune_bot)
- [Inline calculator bot](#inline_calculator_bot)
- [Censored bot](#api_v3_bot)
- [FSM bot](#fsm_bot)

---

<a name='guess_tune_bot'></a>
#### [Guess tune bot](./guess_tune_bot) - Бот "Угадай мелодию"

- После ввода команды `/game` бот отправляет аудио-фрагмент вместе с клавиатурой
с 4 вариантами ответа:
![result of `/game`](./readme_static/guess_tune_bot.png)

- После выбора одного из вариантов, бот обьявляет результат:
![lose](./readme_static/guess_tune_bot_r0.png)
![win](./readme_static/guess_tune_bot_r1.png)

- Так же по команде `/stats` пользователь может узнать
статистику по количеству игр и проценту побед:
![show stats](./readme_static/guess_tune_bot_stats.png)

---

<a name='inline_calculator_bot'></a>
#### [Inline calculator bot](./inline_calculator_bot/) - Инлайновый бот калькулятор

- В группе с добавленным ботом или в чате с ботом введите `@name_of_your_bot`,
бот выведет вам инлайновую подсказку с тем что нужно сделать:
![screen](./readme_static/inline_calc_bot1.png)

- После этого попробуйте дописать 2 числа, к примеру 17 и 4, бот предложит
выбрать математическое действие:
![screen](./readme_static/inline_calc_bot2.png)

- После выбора(например деления) в чат отправится результат:
![screen](./readme_static/inline_calc_bot3.png)

- Так же бот учитывает невозможность деления на 0:
![screen](./readme_static/inline_calc_bot4.png)
![screen](./readme_static/inline_calc_bot5.png)

---

<a name='api_v3_bot'></a>
#### [Censored bot](./api_v3_bot/) - Бот-контроль в группах
- Для активации необходимо добавить бота в группу и дать ему права на удаление
сообщений, а так же бан пользователей.

- После этого бот будет раздавать баны за запрещенные
в чате фразы, а так же удалять сообщения содержащие ссылки.
Демонстрация работы бота приведена в ролике:
[<p align="center"><img src="./readme_static/click_2_play.png" width="65%"></p>](./readme_static/ban.mp4)


---

<a name='fsm_bot'></a>
#### [FSM bot](./fsm_bot/) - Бот сбора данных с валидацией
- Иногда хочется получить от пользователя какие-то данные
(к примеру данные о заказе: имя, адрес доставки, номер телефона, итд) и проверить
их валидность. Однако в случае с ботом не совсем понятно как это сделать,
форм же нет. Контроль состояний осуществляемый алгоритмом "Конечный автомат",
может с этим помочь. Демонстрация работы бота приведена в ролике:
[<p align="center"><img src="./readme_static/click_2_play.png" width="65%"></p>](./readme_static/fsm.mp4)

---

## Setup
Для запуска необходимо:
1. Скачать репо и установить зависимости:
```
git clone https://gitlab.com/inauris/pytelegrambotapi_bots.git
cd pytelegrambotapi_bots
virtualenv venv
source ./venv/bin/activate
pip install -r requirements.txt
```
2. В корне проекта создать файл `.env` со следующим содержимым:
```
# Bot token from @BotFather
TOKEN = 'YOUR_BOT_TOKEN'
```
3. Запустить ботов:
```
# Бот "угадай мелодию"
python guess_tune_bot/server.py

# Бот инлайн калькулятор
python inline_calculator_bot/server.py

# Бот-контроль в группах
python api_v3_bot/censored_group_bot.py

# Бот для сбора данных с валидацией
python fsm_bot/server.py
```
> p.s. не забудьте выполнить некоторые настройки для каждого конкретного бота
(Указаны в `` в соответствующих директориях с ботом)
