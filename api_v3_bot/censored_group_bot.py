import json
from os.path import dirname, join
from time import time

import telebot

import config

bot = telebot.TeleBot(config.TOKEN)

with open(join(dirname(__file__), "punishment_reasons.json")) as f:
    punishment_reasons = json.load(f)

GROUP_ID = -1001594302160  # Ваш ID группы


def get_language(lang_code):
    if not lang_code:
        return "en"
    if "-" in lang_code:
        lang_code = lang_code.split("-")[0]
    if lang_code == "ru":
        return "ru"
    else:
        return "en"


# Delete all links
@bot.message_handler(
    func=lambda message: message.entities is not None
    and message.chat.id == GROUP_ID
)
def delete_links(message):
    for entity in message.entities:
        # url - обычная ссылка, text_link - ссылка, скрытая под текстом
        if entity.type in ["url", "text_link"]:
            # Мы можем не проверять chat.id, он проверяется ещё в хэндлере
            bot.delete_message(message.chat.id, message.message_id)
        else:
            return


# Read-only ban by 10 minutes
restricted_messages = ["да пошел ты", "fuck you", "я токсик", "i am toxic", "я веган", "i am vegan"]


@bot.message_handler(
    func=lambda message: message.text is not None
    and message.text.lower() in restricted_messages
    and message.chat.id == GROUP_ID
)
def set_ro(message):
    bot.restrict_chat_member(
        message.chat.id, message.from_user.id, until_date=time() + 600
    )
    bot.send_message(
        message.chat.id,
        punishment_reasons.get(
            get_language(message.from_user.language_code)
        ).get("ro_msg"),
        reply_to_message_id=message.message_id,
    )


if __name__ == "__main__":
    bot.infinity_polling()
