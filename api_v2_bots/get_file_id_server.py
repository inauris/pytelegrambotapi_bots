"""
Start this server and run the command /get_file_id to get the file_id of your
images. (Put the photos in the "pictures/" directory beforehand.)
"""

import os
from os.path import dirname, join

import telebot

import config

bot = telebot.TeleBot(config.TOKEN)


@bot.message_handler(commands=["get_file_id"])
def find_file_ids(message):
    pic_dir = join(dirname(__file__), "pictures/")
    for file in os.listdir(pic_dir):
        if file.split(".")[-1] == "jpg" or file.split(".")[-1] == "png":
            f = open(pic_dir + file, "rb")
            msg = bot.send_photo(message.chat.id, f, None)
            # А теперь отправим вслед за файлом его file_id
            bot.send_message(
                message.chat.id,
                f"`{msg.photo[0].file_id}`",
                reply_to_message_id=msg.message_id,
                parse_mode="MARKDOWN",
            )


if __name__ == "__main__":
    bot.infinity_polling()
