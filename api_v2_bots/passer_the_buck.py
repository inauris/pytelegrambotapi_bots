import config
import telebot
from telebot import types

bot = telebot.TeleBot(config.TOKEN)


@bot.message_handler(func=lambda message: True)
def any_message(message):
    bot.reply_to(message, f"Сам {message.text}")


@bot.edited_message_handler(func=lambda message: True)
def edit_message(message):
    bot.edit_message_text(
        chat_id=message.chat.id,
        text=f"Сам {message.text}",
        message_id=message.message_id + 1,
    )


if __name__ == "__main__":
    bot.infinity_polling()
