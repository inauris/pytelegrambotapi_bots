import config
import telebot
from telebot import types

bot = telebot.TeleBot(config.TOKEN)


# Обычный режим
@bot.message_handler(content_types=["text"])
def any_msg(message):
    keyboard = types.InlineKeyboardMarkup()
    callback_button = types.InlineKeyboardButton(
        # Switch-button
        # text="Нажми меня", switch_inline_query="Telegram"
        # Url-button
        # text="Резюме", url="https://speskov2000.github.io/"
        # Callback-button
        text="Нажми меня", callback_data="chat_id"
    )
    keyboard.add(callback_button)
    bot.send_message(
        message.chat.id,
        "Я – сообщение из обычного режима",
        reply_markup=keyboard,
    )


# Инлайн-режим с непустым запросом
@bot.inline_handler(lambda query: len(query.query) > 0)
def query_text(query):
    kb = types.InlineKeyboardMarkup()
    # Добавляем колбэк-кнопку с содержимым "test"
    callback_button = types.InlineKeyboardButton(
        text="Нажми меня", callback_data="inline_message_id"
    )
    kb.add(callback_button)
    single_msg = types.InlineQueryResultArticle(
        id="1",
        title="Press me",
        input_message_content=types.InputTextMessageContent(
            message_text="Я – сообщение из инлайн-режима"
        ),
        reply_markup=kb,
    )
    bot.answer_inline_query(query.id, [single_msg])


@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    # Если сообщение из чата с ботом
    if call.data == "chat_id":
        bot.edit_message_text(
            chat_id=call.message.chat.id,
            message_id=call.message.message_id,
            text="Пыщь",
        )
        bot.answer_callback_query(
            callback_query_id=call.id, show_alert=False, text="Пыщь!"
        )

    # Если сообщение из инлайн-режима
    if call.data == "inline_message_id":
        bot.edit_message_text(
            inline_message_id=call.inline_message_id, text="Бдыщь"
        )
        bot.answer_callback_query(
            callback_query_id=call.id, show_alert=True, text="Бдыщь!"
        )


if __name__ == "__main__":
    bot.infinity_polling()
