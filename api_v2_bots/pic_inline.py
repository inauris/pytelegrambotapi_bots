import config
import telebot
from telebot import types

bot = telebot.TeleBot(config.TOKEN)


@bot.inline_handler(func=lambda query: True)
def inline_mode(query):
    pavuk1 = types.InlineQueryResultCachedPhoto(
        id="1",
        photo_file_id="AgACAgIAAx0EZNahQgACExpii0t6Al2Yheki7DZhgmHxuwFTkwACdroxG5MvYEgWz4GThVomlAEAAwIAA3MAAyQE",
        caption="Это павук №1",
    )
    pavuk2 = types.InlineQueryResultCachedPhoto(
        id="2",
        photo_file_id="AgACAgIAAx0EZNahQgACExxii0t7cCsMkH3nAVTbETjeog9wzQACd7oxG5MvYEjQy_TpRxzUjQEAAwIAA3MAAyQE",
        caption="Это павук №2",
    )
    pavuk3 = types.InlineQueryResultCachedPhoto(
        id="3",
        photo_file_id="AgACAgIAAx0EZNahQgACEx5ii0t703izNCsUsqEeS7KfvmqxtQACfLoxG5MvYEjTtGKXzDskmwEAAwIAA3MAAyQE",
        caption="Это павук №3",
    )
    bot.answer_inline_query(query.id, [pavuk1, pavuk2, pavuk3])


if __name__ == "__main__":
    bot.infinity_polling()
