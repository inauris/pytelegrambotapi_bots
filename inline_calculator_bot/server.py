import config
import re
import telebot
from telebot import types

bot = telebot.TeleBot(config.TOKEN)


digits_pattern = re.compile(r"^\d+ \d+$", re.MULTILINE)

plus_icon = "https://pp.vk.me/c627626/v627626512/2a627/7dlh4RRhd24.jpg"
minus_icon = "https://pp.vk.me/c627626/v627626512/2a635/ILYe7N2n8Zo.jpg"
divide_icon = "https://pp.vk.me/c627626/v627626512/2a620/oAvUk7Awps0.jpg"
multiply_icon = "https://pp.vk.me/c627626/v627626512/2a62e/xqnPMigaP5c.jpg"
error_icon = "https://pp.vk.me/c627626/v627626512/2a67a/ZvTeGq6Mf88.jpg"


@bot.inline_handler(func=lambda query: len(query.query) == 0)
def empty_query(query):
    try:
        r = types.InlineQueryResultArticle(
            id="1",
            title='Бот "Super Calculating 3000"',
            description="Введите ровно 2 числа и получите результат!",
            input_message_content=types.InputTextMessageContent(
                message_text="Надо было ввести 2 числа -_-"
            ),
        )
        bot.answer_inline_query(query.id, [r])
    except Exception as e:
        print(e)


@bot.inline_handler(func=lambda query: len(query.query) > 0)
def query_text(query):
    try:
        matches = re.match(digits_pattern, query.query)
        if not matches:
            return
    except AttributeError:
        return

    num1, num2 = matches.group().split()
    try:
        m_sum = int(num1) + int(num2)
        r_sum = types.InlineQueryResultArticle(
            id="1",
            title="Сумма",
            description=f"Результат: {m_sum}",
            input_message_content=types.InputTextMessageContent(
                message_text=f"{num1} + {num2} = {m_sum}"
            ),
            thumb_url=plus_icon,
            thumb_width=48,
            thumb_height=48,
        )
        m_sub = int(num1) - int(num2)
        r_sub = types.InlineQueryResultArticle(
            id="2",
            title="Разность",
            description=f"Результат: {m_sub}",
            input_message_content=types.InputTextMessageContent(
                message_text=f"{num1} - {num2} = {m_sub}"
            ),
            thumb_url=minus_icon,
            thumb_width=48,
            thumb_height=48,
        )
        # Учтем деление на ноль и подготовим 2 варианта развития событий
        if num2 != "0":
            m_div = int(num1) / int(num2)
            r_div = types.InlineQueryResultArticle(
                id="3",
                title="Частное",
                description=f"Результат: {m_div}",
                input_message_content=types.InputTextMessageContent(
                    message_text=f"{num1} / {num2} = {m_div}"
                ),
                thumb_url=divide_icon,
                thumb_width=48,
                thumb_height=48,
            )
        else:
            r_div = types.InlineQueryResultArticle(
                id="3",
                title="Частное",
                description="На ноль делить нельзя!",
                input_message_content=types.InputTextMessageContent(
                    message_text="Я нехороший человек и делю на ноль!"
                ),
                thumb_url=error_icon,
                thumb_width=48,
                thumb_height=48,
            )
        m_mul = int(num1) * int(num2)
        r_mul = types.InlineQueryResultArticle(
            id="4",
            title="Произведение",
            description=f"Результат: {m_mul}",
            input_message_content=types.InputTextMessageContent(
                message_text=f"{num1} * {num2} = {m_mul}"
            ),
            thumb_url=multiply_icon,
            thumb_width=48,
            thumb_height=48,
        )
        bot.answer_inline_query(
            query.id, [r_sum, r_sub, r_div, r_mul], cache_time=2147483646
        )
    except Exception as e:
        print("{!s}\n{!s}".format(type(e), str(e)))


if __name__ == "__main__":
    bot.infinity_polling()
