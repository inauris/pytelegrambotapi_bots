import telebot

import config
import DBManager
from config import States

bot = telebot.TeleBot(config.TOKEN)


# Начало диалога
@bot.message_handler(commands=["start"])
def cmd_start(message):
    state = DBManager.get_current_state(message.chat.id)
    if state == States.S_NAME.value:
        bot.send_message(
            message.chat.id,
            "Кажется кто-то обещал отправить свое имя,"
            " но так и не сделал этого :( \n(жду)..."
        )
    elif state == States.S_AGE.value:
        bot.send_message(
            message.chat.id,
            "Кажется кто-то обещал отправить свой возраст,"
            " но так и не сделал этого :( \n(жду)..."
        )
    elif state == States.S_PIC.value:
        bot.send_message(
            message.chat.id,
            "Кажется кто-то обещал отправить фото,"
            " но так и не сделал этого :( \n(жду)..."
        )
    else:
        bot.send_message(
            message.chat.id, "Привет! Как я могу к тебе обращаться?"
        )
        DBManager.set_state(message.chat.id, States.S_NAME.value)


# По команде /reset будем сбрасывать состояния, возвращаясь к началу диалога
@bot.message_handler(commands=["reset"])
def cmd_reset(message):
    bot.send_message(
        message.chat.id, "Что ж, начнём по-новой. Как тебя зовут?"
    )
    DBManager.set_state(message.chat.id, States.S_NAME.value)


@bot.message_handler(commands=["stop"])
def stop_dialogue(message):
    if DBManager.del_state(message.chat.id):
        bot.send_message(
            message.chat.id, "Тогда завершим."
            "Если захочешь пообщаться снова - отправь команду /start.",
        )
    else:
        return


@bot.message_handler(commands=["state"])
def get_state(message):
    state = DBManager.get_current_state(message.chat.id)
    bot.send_message(message.chat.id, state)


@bot.message_handler(
    func=lambda message: DBManager.get_current_state(message.chat.id)
    == States.S_NAME.value
)
def user_entering_name(message):
    # Name without validate
    bot.send_message(
        message.chat.id,
        "Отличное имя, запомню! Теперь укажи, пожалуйста, свой возраст.",
    )
    DBManager.set_state(message.chat.id, States.S_AGE.value)


@bot.message_handler(
    func=lambda message: DBManager.get_current_state(message.chat.id)
    == States.S_AGE.value
)
def user_entering_age(message):
    if message.text.isdigit():
        if int(message.text) in range(5, 100):
            bot.send_message(
                message.chat.id,
                "Возраст супер! Теперь отправь фотку, пожалуйста",
            )
            DBManager.set_state(message.chat.id, States.S_PIC.value)
        else:
            bot.send_message(
                message.chat.id,
                "Какой-то странный возраст."
                "Не верю! Отвечай честно."
            )
            return
    else:
        bot.send_message(message.chat.id, "Пиши просто цифру",)
        return


@bot.message_handler(
    content_types=["photo"],
    func=lambda message: DBManager.get_current_state(message.chat.id)
    == States.S_PIC.value,
)
def user_sending_photo(message):
    # Pics validate in handler
    bot.send_message(
        message.chat.id,
        "Отлично! Больше от тебя ничего не требуется."
        "Если захочешь пообщаться снова - отправь команду /start.",
    )
    DBManager.del_state(message.chat.id)


@bot.message_handler(
    func=lambda message: DBManager.get_current_state(message.chat.id)
    == States.S_PIC.value,
)
def user_sending_not_photo(message):
    bot.send_message(
        message.chat.id,
        "Надо было отправить картинку! Отправь еще раз!!!",
    )
    return


if __name__ == "__main__":
    bot.infinity_polling()
