from vedis import Vedis
import config


def get_current_state(user_id):
    with Vedis(config.DB_FILE) as db:
        try:
            return db[user_id].decode()
        except KeyError:
            return config.States.S_START.value


def set_state(user_id, value):
    with Vedis(config.DB_FILE) as db:
        db[user_id] = value


def del_state(user_id):
    with Vedis(config.DB_FILE) as db:
        try:
            db.delete(user_id)
            return True
        except KeyError:
            return False
