from enum import Enum
from os import getenv
from os.path import dirname, join

from dotenv import load_dotenv

load_dotenv(join(dirname(__file__), "../.env"))

TOKEN = getenv("TOKEN")
DB_FILE = join(dirname(__file__), "database.vdb")


class States(Enum):
    """
    Vedis contain only strings, so we use str as value
    """
    S_START = "Waiting for the any commands (as start)"
    S_NAME = "Waiting for the name to be entered"
    S_AGE = "Waiting for the age to be entered"
    S_PIC = "Waiting for a picture"
