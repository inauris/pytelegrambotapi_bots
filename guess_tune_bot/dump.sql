PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE music (file_id VARCHAR (100) PRIMARY KEY, right_answer VARCHAR (100), wrong_answers VARCHAR (300));
INSERT INTO music VALUES('AwACAgIAAxkDAAM7YnpPAAF8AgQtBeD7sTJLiHU5499cAAKoGAACeDvYS21FmZrCpPMyJAQ','Hold It Right There','Magic Time,Travelin'' Light,Everyday I Have the Blues');
INSERT INTO music VALUES('AwACAgIAAxkDAAM9YnpPBAT97w-axvma_XUq0imd2QwAAqkYAAJ4O9hLqgwHGbVt91ckBA','All Saints Day','Close Enough for Jazz,Goldfish Bowl,Travelin'' Light');
INSERT INTO music VALUES('AwACAgIAAxkDAAM_YnpPCETLZkfL02P31NHxwII71WcAAqoYAAJ4O9hLAAHRoOKDlxUNJAQ','Miss Otis Regrets','Celtic Swing,Sticks and Stones,Hold It Right There');
INSERT INTO music VALUES('AwACAgIAAxkDAANCYns3s3MpkKY4yWFyNhhSmN49xGUAAtsXAAJDzeBLcxB1N87fWS0kBA','The Things I Used to Do','Close Enough for Jazz,Evening Shadows,You''re Driving Me Crazy');
INSERT INTO music VALUES('AwACAgIAAxkDAANEYns3tzYJsKZ5GvIUh7ho6el5C3kAAtwXAAJDzeBLtAVrfg3lDqAkBA','The Way Young Lovers Do','Have I Told You Lately,Hold It Right There,Celtic Swing');
COMMIT;
