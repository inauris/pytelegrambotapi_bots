from os import getenv
from os.path import dirname, join

from dotenv import load_dotenv

load_dotenv(join(dirname(__file__), "../.env"))

TOKEN = getenv("TOKEN")

# Sqlite database
DB_NAME = join(dirname(__file__), "music.db")

# Shelve databases
SESSION_SHELVE = join(dirname(__file__), "session_shelve.db")
STATS_SHELVE = join(dirname(__file__), "stats_shelve.db")
