import telebot

import config
import utils
from DBManager import DBManager

bot = telebot.TeleBot(config.TOKEN)


@bot.message_handler(commands=["game"])
def game(message):
    # Connect to the database
    db_worker = DBManager(config.DB_NAME)
    # select_single() returns a random entry from the database
    entry = db_worker.select_single()
    # Send an audio message with answer options
    markup = utils.generate_markup(entry[1], entry[2])
    bot.send_voice(message.chat.id, entry[0], reply_markup=markup)
    # Opening a game session
    client_identification = f"{message.chat.id}:{message.from_user.id}"
    utils.set_user_game(client_identification, entry[1])
    db_worker.close()


@bot.message_handler(commands=["stats"])
def stats(message):
    stats = utils.get_stats(message.from_user.id)
    if not stats:
        bot.send_message(
            message.chat.id,
            "Вы не сыграли ни одной игры."
            + " Срочно исправьте! Введите команду /game",
        )
    else:
        number_of_games = stats[0]
        win_percentage = stats[1]
        form_message = (
            f"Вы сыграли {number_of_games} игр,"
            + f" выигрышей: {round(win_percentage, 2)}%!"
        )
        bot.send_message(message.chat.id, form_message)


@bot.message_handler(content_types=["text"])
def check_answer(message):
    # Checking an open session
    client_identification = f"{message.chat.id}:{message.from_user.id}"
    answer = utils.get_answer_for_user(client_identification)
    if not answer:
        bot.send_message(
            message.chat.id, "Чтобы начать игру " + "выберите команду /game"
        )
    else:
        keyboard_hider = telebot.types.ReplyKeyboardRemove()
        # Checking the correct answer
        if message.text == answer:
            utils.fill_stats(message.from_user.id, True)
            bot.send_message(
                message.chat.id, "Верно!", reply_markup=keyboard_hider
            )
        else:
            utils.fill_stats(message.from_user.id, False)
            bot.send_message(
                message.chat.id,
                "Увы, Вы не угадали. Попробуйте ещё раз!",
                reply_markup=keyboard_hider,
            )
        # Closing the game session
        utils.finish_user_game(client_identification)


if __name__ == "__main__":
    bot.infinity_polling()
