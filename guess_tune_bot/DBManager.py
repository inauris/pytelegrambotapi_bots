import sqlite3


class DBManager:
    def __init__(self, database):
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()

    def select_single(self):
        """Получаем одну строку с номером rownum"""
        with self.connection:
            return self.cursor.execute(
                "SELECT * FROM music ORDER BY RANDOM()\
                    LIMIT 1;"
            ).fetchall()[0]

    def close(self):
        """Закрываем текущее соединение с БД"""
        self.connection.close()
