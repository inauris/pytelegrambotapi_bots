"""
Start this server and run the command /get_file_id to get the file_id of your
audio recordings. (Put the music in the "music/" directory beforehand.)
"""

import os
from os.path import dirname, join

import telebot

import config

bot = telebot.TeleBot(config.TOKEN)


@bot.message_handler(commands=["get_file_id"])
def find_file_ids(message):
    music_dir = join(dirname(__file__), "music/")
    for file in os.listdir(music_dir):
        if file.split(".")[-1] == "ogg":
            f = open(music_dir + file, "rb")
            msg = bot.send_voice(message.chat.id, f, None)
            # А теперь отправим вслед за файлом его file_id
            bot.send_message(
                message.chat.id,
                f"`{msg.voice.file_id}`",
                reply_to_message_id=msg.message_id,
                parse_mode="MARKDOWN",
            )


if __name__ == "__main__":
    bot.infinity_polling()
