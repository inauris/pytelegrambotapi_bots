import random
import shelve

from telebot.types import ReplyKeyboardMarkup

from config import SESSION_SHELVE, STATS_SHELVE


def set_user_game(client_identification: str, right_answer: str) -> None:
    """Opening a game session"""
    with shelve.open(SESSION_SHELVE) as storage:
        storage[client_identification] = right_answer


def finish_user_game(client_identification: str) -> None:
    """Closing the game session"""
    with shelve.open(SESSION_SHELVE) as storage:
        del storage[client_identification]


def get_answer_for_user(client_identification: str):
    """Get right answer or None"""
    with shelve.open(SESSION_SHELVE) as storage:
        return storage.get(client_identification)


def fill_stats(user_id, result):
    """
    :result: bool (win or lose)
    Create or update object in stats_shelve.db
    object: {"user_id": list(number_of_games, average)}
    """
    with shelve.open(STATS_SHELVE) as storage:
        stat = storage.get(str(user_id))
        if not stat:
            if result:
                storage[str(user_id)] = [1, 100]
            else:
                storage[str(user_id)] = [1, 0]
        else:
            # Victory = 100. The value of total wins (average*number_of_games)
            value_of_total_wins = stat[1] * stat[0]
            number_of_games = stat[0] + 1
            if result:
                average = (value_of_total_wins + 100) / number_of_games
                storage[str(user_id)] = [number_of_games, average]
            else:
                average = value_of_total_wins / number_of_games
                storage[str(user_id)] = [number_of_games, average]


def get_stats(user_id):
    """
    Try to get object from stats_shelve.db
    object: {"user_id": list(number_of_games, average)}
    """
    with shelve.open(STATS_SHELVE) as storage:
        return storage.get(str(user_id))


def generate_markup(right_answer, wrong_answers) -> ReplyKeyboardMarkup:
    markup = ReplyKeyboardMarkup(
        one_time_keyboard=True, resize_keyboard=True, selective=True
    )
    # Creating List with all answers
    list_items = [right_answer]
    for item in wrong_answers.split(","):
        list_items.append(item)
    random.shuffle(list_items)
    # Pulling markup
    for item in list_items:
        markup.add(item)
    return markup
