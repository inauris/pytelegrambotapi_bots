from os.path import dirname, join

import cherrypy
import telebot

import config

WEBHOOK_HOST = "94.141.238.94"
# 443, 80, 88 или 8443 (порт должен быть открыт!)
WEBHOOK_PORT = 8443
# На некоторых серверах придется указывать такой же IP, что и выше
# WEBHOOK_LISTEN = "0.0.0.0"
WEBHOOK_LISTEN = WEBHOOK_HOST

# Certificate and private key
WEBHOOK_SSL_CERT = join(dirname(__file__), "./webhook_cert.pem")
WEBHOOK_SSL_PRIV = join(dirname(__file__), "./webhook_pkey.pem")

WEBHOOK_URL_BASE = f"https://{WEBHOOK_HOST}/{WEBHOOK_PORT}"
WEBHOOK_URL_PATH = f"/{config.TOKEN}/"

bot = telebot.TeleBot(config.TOKEN)


# Наш вебхук-сервер
class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        if (
            "content-length" in cherrypy.request.headers
            and "content-type" in cherrypy.request.headers
            and cherrypy.request.headers["content-type"] == "application/json"
        ):
            length = int(cherrypy.request.headers["content-length"])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            update = telebot.types.Update.de_json(json_string)
            # Эта функция обеспечивает проверку входящего сообщения
            bot.process_new_updates([update])
            return ""
        else:
            raise cherrypy.HTTPError(403)


@bot.message_handler(func=lambda message: True, content_types=["text"])
def repeat_all_messages(message):
    bot.send_message(message.chat.id, message.text)


bot.remove_webhook()

bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH,
                certificate=open(WEBHOOK_SSL_CERT, 'r'))

# Указываем настройки сервера CherryPy
cherrypy.config.update({
    'server.socket_host': WEBHOOK_LISTEN,
    'server.socket_port': WEBHOOK_PORT,
    'server.ssl_module': 'builtin',
    'server.ssl_certificate': WEBHOOK_SSL_CERT,
    'server.ssl_private_key': WEBHOOK_SSL_PRIV
})

cherrypy.quickstart(WebhookServer(), WEBHOOK_URL_PATH, {'/': {}})
